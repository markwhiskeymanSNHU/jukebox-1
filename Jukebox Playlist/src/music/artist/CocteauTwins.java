/*
 * Author: Alex Hamilton
 * 
 * Cocteau Twins were a Scottish gothic rock and dream pop band whose discography contains nine studio albums, seven compilations, and numerous EPs and singles
 */
package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class CocteauTwins {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public CocteauTwins() {
    }
    
    //Songs from 1990 album "Heaven or Las Vegas"
    public ArrayList<Song> getCocteauTwinsSongs() {
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Cherry-coloured Funk", "Cocteau Twins");       //Create a song
         Song track2 = new Song("Heaven Or Las Vegas", "Cocteau Twins");        //Create another song
         Song track3 = new Song("Iceblink Luck", "Cocteau Twins");				//Create a third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Cocteau Twins
         this.albumTracks.add(track2);                                          //Add the second song to song list for Cocteau Twins
         this.albumTracks.add(track3);											//Add the third song to song list for Cocteau Twins
         return albumTracks;                                                    //Return the songs for Cocteau Twins in the form of an ArrayList
    }
}